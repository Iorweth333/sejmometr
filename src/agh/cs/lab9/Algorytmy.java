package agh.cs.lab9;

import static java.lang.System.out;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.LinkedList;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.internal.LinkedTreeMap;

public class Algorytmy {

	/*
	 * suma wydatków posła/posłanki o określonym imieniu i nazwisku wysokości
	 * wydatków na 'drobne naprawy i remonty biura poselskiego' określonego
	 * posła/posłanki średniej wartości sumy wydatków wszystkich posłów
	 * posła/posłanki, który wykonał najwięcej podróży zagranicznych
	 * posła/posłanki, który najdłużej przebywał za granicą posła/posłanki,
	 * który odbył najdroższą podróż zagraniczną listę wszystkich posłów, którzy
	 * odwiedzili Włochy
	 */

	public static void wydatkiPosla(Posel posel, LinkedList<Dataobject> listaPoslow) {

		// DEBUG
		// System.out.println("Szukam wydatków: " + posel);

		for (int i = 0; i < listaPoslow.size(); i++) {

			// DEBUG
			// out.print("Sprawdzam " + listaPoslow.get(i).getId() + " ");

			Data dane = listaPoslow.get(i).getData();

			// DEBUG
			// out.println(dane.getImie() + " " + dane.getNazwisko());

			if (dane.getImie().equals(posel.imie) && dane.getNazwisko().equals(posel.nazwisko)) {
				posel.id = dane.getId();
				break;
			}
		}

		if (posel.id.equals("-1")) {
			out.println("Nie znaleziono takiego posła w tej kadencji");
			System.exit(1);
		}

		Czytacz czytacz = null;

		try {
			czytacz = new Czytacz(
					"https://api-v3.mojepanstwo.pl/dane/poslowie/" + posel.id + ".json?layers[]=krs&layers[]=wydatki",
					"wydatki");
		} catch (MalformedURLException e) {
			System.out.println("Problem z linkiem podczas pobierania wydatków");
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			System.out.println("Problem z plikiem podczas pobierania wydatków");
			e.printStackTrace();
			System.exit(1);
		}

		/*
		 * eclipse krzyczał, że dead code no to wykomentowałem if (czytacz ==
		 * null) {
		 * System.out.println("Cos poszło nie tak z pobieraniem wydatków");
		 * return 0; }
		 */

		Wydatki wydatki = czytacz.wydatki;

		Roczniki[] roczniki = wydatki.getRoczniki();

		// DEBUG
		// System.out.println("jestem tutaj");

		for (int i = 0; i < wydatki.getLiczba_rocznikow(); i++) {
			for (int j = 0; j < wydatki.getLiczba_pol(); j++) {
				posel.wydatkiSuma += roczniki[i].getPola(j);

				// DEBUG
				// System.out.println(wydatki.getPunkty()[j].getTytul());

				String tytul = wydatki.getPunkty()[j].getTytul();

				if (tytul.equals("Koszty drobnych napraw i remontów lokalu biura poselskiego"))
					posel.drobneNaprawy += roczniki[i].getPola(j);

			}
		}

		// DEBUG
		// out.println(posel);
	}

	public static float podliczSrednieWydatki(LinkedList<Dataobject> listaPoslow)
			throws MalformedURLException, IOException {

		float wydatkiSuma = 0;

		Czytacz.pobierzPlikiWarstw(listaPoslow, "wydatki", false); // false bo
																	// to nie
																	// aktualizacja
		// uruchamiam na wypadek jakby brakowało jakiegoś pliku

		for (Dataobject posel : listaPoslow) {

			String id = posel.getId();

			// DEBUG
			out.println("Sprawdzam " + id + " ");

			Gson gson = new Gson();

			BufferedReader bufor = null;

			try {
				bufor = new BufferedReader(new FileReader("./PobraneDane/wydatkiPoslaID" + id + ".json"));
			} catch (FileNotFoundException e) {
				System.out.println("Cos poszło nie tak przy odczycie pliku wydatkiPoslaID" + id + ".json");
				e.printStackTrace();
				System.exit(1);
			}

			WarstwyGlownaKlasa warstwyG = gson.fromJson(bufor, WarstwyGlownaKlasa.class);

			Wydatki wydatki = warstwyG.getLayers().getWydatki();

			try {
				bufor.close();
			} catch (IOException e) {
				System.out.println("Cos poszło nie tak przy zamykaniu pliku wydatkiPoslaID" + id + ".json");
				e.printStackTrace();
				System.exit(1);
			}

			Roczniki[] roczniki = wydatki.getRoczniki();

			for (int i = 0; i < wydatki.getLiczba_rocznikow(); i++) {
				for (int j = 0; j < wydatki.getLiczba_pol(); j++) {
					wydatkiSuma += roczniki[i].getPola(j);
				}
			}

		}
		return wydatkiSuma / listaPoslow.size();
	}

	public static Posel znajdzPodroznika(LinkedList<Dataobject> listaPoslow, String polecenie)
			throws MalformedURLException, IOException {

		Czytacz.pobierzPlikiWarstw(listaPoslow, "wyjazdy", false); // false bo
																	// to nie
																	// aktualizacja
		// uruchamiam na wypadek jakby brakowało jakiegoś pliku

		Posel szukanyPosel = new Posel(-1, -1, -1); // (int liczbaPodrozy, int
													// dniZaGranica, float
													// najdrozszaPodroz)

		for (Dataobject posel : listaPoslow) {

			String id = posel.getId();

			// DEBUG
			// out.println("Sprawdzam " + id + " ");

			Gson gson = new Gson();

			BufferedReader bufor = null;

			try {
				bufor = new BufferedReader(new FileReader("./PobraneDane/wyjazdyPoslaID" + id + ".json"));
			} catch (FileNotFoundException e) {
				System.out.println("Cos poszło nie tak przy odczycie pliku wyjazdyPoslaID" + id + ".json");
				e.printStackTrace();
				System.exit(1);
			}

			WarstwyGlownaKlasa warstwyG = null;
			try {
				warstwyG = gson.fromJson(bufor, WarstwyGlownaKlasa.class);
			} catch (Exception e1) {
				// System.out.println("błąd- nie było wyjazdów");
				warstwyG = null;
			}

			ArrayList<Wyjazdy> wyjazdy = null;

			if (warstwyG != null)
				wyjazdy = (ArrayList<Wyjazdy>) warstwyG.getLayers().getWyjazdy();

			// kalekie API
			// jeśli nie ma żadnych wyjazdów to zamiast pustej tablicy jest
			// pusty obiekt
			// i gson protestuje że sie typy nie zgadzają
			// nie ma rady, trzeba to obejść

			// metodą prób i błędów (wielu....) a takze poprzez wywolanie metod
			// getClass
			// dowiedziałem się że jeśli nie będzie wyjazdów to gson zwróci
			// LinkedTreeMap
			// wystarczy omijać ten przypadek szerokim łukiem i jest lux

			try {
				bufor.close();
			} catch (IOException e) {
				System.out.println("Cos poszło nie tak przy zamykaniu pliku wydatkiPoslaID" + id + ".json");
				e.printStackTrace();
				System.exit(1);
			}

			if (wyjazdy != null) {

				int liczbaPodrozy = wyjazdy.size();

				if (polecenie.equals("najwiecejPodrozy") && liczbaPodrozy > szukanyPosel.liczbaPodrozy) {
					szukanyPosel.liczbaPodrozy = liczbaPodrozy;
					szukanyPosel.id = posel.getId();
					szukanyPosel.imie = posel.getData().getImie();
					szukanyPosel.nazwisko = posel.getData().getNazwisko();
				}

				if (polecenie.equals("najdluzejZaGranica")) {

					int dniZaGranica = 0;

					for (Wyjazdy wyjazd : wyjazdy) {
						dniZaGranica += wyjazd.getLiczba_dni();
					}

					if (dniZaGranica > szukanyPosel.dniZaGranica) {
						szukanyPosel.dniZaGranica = dniZaGranica;
						szukanyPosel.id = posel.getId();
						szukanyPosel.imie = posel.getData().getImie();
						szukanyPosel.nazwisko = posel.getData().getNazwisko();
					}

				}

				if (polecenie.equals("najdrozszaPodroz")) {

					for (Wyjazdy wyjazd : wyjazdy) {
						if (wyjazd.getKoszt_suma() > szukanyPosel.najdrozszaPodroz) {
							szukanyPosel.najdrozszaPodroz = wyjazd.getKoszt_suma();
							szukanyPosel.id = posel.getId();
							szukanyPosel.imie = posel.getData().getImie();
							szukanyPosel.nazwisko = posel.getData().getNazwisko();
						}
					}
				}

			}

		}

		return szukanyPosel;

	}

	public static LinkedList<Posel> znajdzOdwiedzajacychWlochy(LinkedList<Dataobject> listaPoslow)
			throws MalformedURLException, IOException {

		LinkedList<Posel> odwiedziliWlochy = new LinkedList<Posel>();

		Czytacz.pobierzPlikiWarstw(listaPoslow, "wyjazdy", false);

		for (Dataobject posel : listaPoslow) {

			String id = posel.getId();

			Gson gson = new Gson();

			BufferedReader bufor = null;

			try {
				bufor = new BufferedReader(new FileReader("./PobraneDane/wyjazdyPoslaID" + id + ".json"));
			} catch (FileNotFoundException e) {
				System.out.println("Cos poszło nie tak przy odczycie pliku wyjazdyPoslaID" + id + ".json");
				e.printStackTrace();
				System.exit(1);
			}

			WarstwyGlownaKlasa warstwyG = null;
			try {
				warstwyG = gson.fromJson(bufor, WarstwyGlownaKlasa.class); // nie
																			// było
																			// wyjazdów
			} catch (Exception e1) {

				warstwyG = null;
			}

			ArrayList<Wyjazdy> wyjazdy = null;

			if (warstwyG != null)
				wyjazdy = (ArrayList<Wyjazdy>) warstwyG.getLayers().getWyjazdy();

			try {
				bufor.close();
			} catch (IOException e) {
				System.out.println("Cos poszło nie tak przy zamykaniu pliku wydatkiPoslaID" + id + ".json");
				e.printStackTrace();
				System.exit(1);
			}

			if (wyjazdy != null) {

				for (Wyjazdy wyjazd : wyjazdy) {

					if (wyjazd.getKraj().equals("Włochy")) {

						Posel odwiedzilWlochy = new Posel(posel.getData().getImie(), posel.getData().getNazwisko());

						odwiedziliWlochy.add(odwiedzilWlochy);
						break;
					}

				}

			}

		}

		return odwiedziliWlochy;
	}

}
