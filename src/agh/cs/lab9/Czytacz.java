package agh.cs.lab9;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
//import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;

import org.apache.commons.io.FileUtils;
import com.google.gson.Gson;

public class Czytacz {

	private String link;
	// public String dane;
	// zmiana w stosunku do pierwotnego projektu; dane zostaną zapisane jako
	// obiekt specjalnej klasy a nie jako string
	// po prostu nie miałem wtedy pojącia, ze json to cos więcej niż tekst ;)
	public Poslowie poslowie;
	public Wydatki wydatki;

	// https://api-v3.mojepanstwo.pl/dane/poslowie/174.json?layers[]=krs&layers[]=wydatki?conditions[imie]=Adam&conditions[nazwisko]=Abramowicz

	public Czytacz(String link, String kadencja, boolean aktualizacja) throws MalformedURLException, IOException {
		this.link = link;
		this.poslowie = pobierz(this.link, kadencja, aktualizacja);
	}

	public Czytacz(String link, String polecenie) throws MalformedURLException, IOException {
		this.link = link;

		if (polecenie.equals("wydatki"))
			this.wydatki = pobierzWydatki(this.link);
	}

	private Poslowie pobierz(String link, String kadencja, boolean aktualizacja)
			throws MalformedURLException, IOException {

		File folder = new File("./PobraneDane"); // za pomoca klasy File można
													// tworzyć również foldery
		if (!folder.exists())
			folder.mkdir(); // sluzy do tego mkdir wlaśnie
							// w katalogu PobraneDane zapiszę json'y z API

		//DEBUG
		//System.out.println(link);
		
		URL url = new URL(link);

		File plik_lista_poslow = new File("./PobraneDane/listaPoslowKadencji" + kadencja + "Str1.json");

		if (!plik_lista_poslow.exists() || aktualizacja == true)
			FileUtils.copyURLToFile(url, plik_lista_poslow, 10000, 10000);
		// znalezione na
		// http://stackoverflow.com/questions/921262/how-to-download-and-save-a-file-from-internet-using-java
		// instrukcja jak użyć FileUtils:
		// https://www.youtube.com/watch?v=XcaIimghqBU
		// (link, plik docelowy, connection timout, readTimeout) w milisekundach

		Gson gson = new Gson();

		BufferedReader bufor = null;

		bufor = new BufferedReader(new FileReader("./PobraneDane/listaPoslowKadencji" + kadencja + "Str1.json"));

		Poslowie wynik = gson.fromJson(bufor, Poslowie.class);

		bufor.close();

		int i = 2; // posłuży do zliczania plików, gdyż dane o posłach są w
					// więcej niż jednym json'ie
		// liczę od 2, bo pierwszy trzeba pobrać osobno, bo linki do kolejnej
		// strony
		// znajdują się na końcu kazdego pliku

		url = new URL(wynik.getLinks().getNext());

		//DEBUG
		//System.out.println("nastepny link: " + url);

		while (url != null) {
			plik_lista_poslow = new File("./PobraneDane/listaPoslowKadencji" + kadencja + "Str" + i + ".json");
			
			//DEBUG
			/*
			if (!plik_lista_poslow.exists() || aktualizacja == true)
				System.out.println("plik nie istnieje lub aktualizacja");
			else
				System.out.println("plik istnieje");
			*/
			
			if (!plik_lista_poslow.exists() || aktualizacja == true)
				FileUtils.copyURLToFile(url, plik_lista_poslow, 10000, 10000);
			bufor = new BufferedReader(
					new FileReader("./PobraneDane/listaPoslowKadencji" + kadencja + "Str" + i + ".json"));
			Poslowie tmp = new Poslowie();
			tmp = gson.fromJson(bufor, Poslowie.class);
			bufor.close();
			wynik.scal(tmp.getListaPoslow());
			if (tmp.getLinks().getNext() != null)
				url = new URL(tmp.getLinks().getNext());
			else
				url = null;
			i++;
			//DEBUG
			//System.out.println("nastepny (" + i + ") link: " + url);
		}

		return wynik;
	}

	private Wydatki pobierzWydatki(String link) throws IOException, MalformedURLException {		//chodzi o wydatki jednego posła
		File folder = new File("./PobraneDane");
		if (!folder.exists())
			folder.mkdir();

		URL url = new URL(link);

		File plik_wydatki_posla = new File("./PobraneDane/wydatkiPosla.json");
		
		FileUtils.copyURLToFile(url, plik_wydatki_posla, 10000, 10000);			//nie zamieszczam if z aktualizacją bo nad jednym tymczasowym pliczkiem to sie nie ma co rozdrabniać

		Gson gson = new Gson();

		BufferedReader bufor = null;

		bufor = new BufferedReader(new FileReader("./PobraneDane/wydatkiPosla.json"));

		WarstwyGlownaKlasa wydatkiG = gson.fromJson(bufor, WarstwyGlownaKlasa.class);

		Wydatki wydatki = wydatkiG.getLayers().getWydatki();
		bufor.close();
		
		plik_wydatki_posla.deleteOnExit();

		//DEBUG
		//System.out.println(wydatki);

		return wydatki;

	}

	public static void pobierzPlikiWarstw(LinkedList<Dataobject> listaPoslow, String warstwa, boolean aktualizacja) throws IOException, MalformedURLException {
		
		File folder = new File("./PobraneDane");
		if (!folder.exists())
			folder.mkdir();
		
		
		for (Dataobject posel : listaPoslow) {
			String id = posel.getId();
			String link = "https://api-v3.mojepanstwo.pl/dane/poslowie/" + id + ".json?layers[]=krs&layers[]=" + warstwa;
			URL url = new URL(link);

			File plik_warstwy_posla = new File("./PobraneDane/" + warstwa + "PoslaID" + id + ".json");

			if (!plik_warstwy_posla.exists() || aktualizacja == true){
				System.out.println("Pobieram "+ warstwa + " posła o ID " + id);
				FileUtils.copyURLToFile(url, plik_warstwy_posla, 10000, 10000);
			}
			// obiektu klasy File nie trzeba zamykać. Czytałem, że File to bardzo nieadekwatna nazwa- powinna raczej brzmieć Path. A ścieżki raczej się nie zamyka ;)			
		}
		
	}
}
