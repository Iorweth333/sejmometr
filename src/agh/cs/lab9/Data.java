package agh.cs.lab9;

import com.google.gson.annotations.SerializedName;

public class Data
{
	/*
	@SerializedName ("poslowie.imie_drugie")
    private String drugieImie;
	*/
	
	@SerializedName ("poslowie.imie_pierwsze")
	private String imie;
	
	@SerializedName ("poslowie.nazwisko")
	private String nazwisko;
	
	@SerializedName ("poslowie.id")
	private String id;
		
	@SerializedName ("poslowie.kadencja")
	private String[] kadencjeTab;
		
	@SerializedName ("poslowie.kadencja_ostatnia")
	private String ostatniaKadencja;
	/*
	@SerializedName ("ludzie.id")	//po co jakieś drugie id?
	private String ludzieId;
	*/
	
	
	
	
	@SerializedName ("poslowie.wartosc_biuro_zlecenia")
	private float biuroZlecenia;
	
	@SerializedName ("poslowie.wartosc_biuro_materialy_biurowe")
	private float materialyBiurowe;
	
	
	@SerializedName ("poslowie.wartosc_biuro_ekspertyzy")
	private float biuroEkspertyzy;
					
	@SerializedName ("poslowie.wartosc_biuro_telekomunikacja")
	private float biuroTelekomunikacja;
	
	@SerializedName ("poslowie.wartosc_biuro_biuro")
	private float biuroBiuro;								//wut??
	
	@SerializedName ("poslowie.liczba_przelotow")
	private float liczbaPrzelotow;
			
	@SerializedName ("poslowie.wartosc_refundacja_kwater_pln")
	private float refundacjaKwater;
					
	@SerializedName ("poslowie.wartosc_biuro_przejazdy")
	private float biuroPrzejazdyWartosc;
									
									
	@SerializedName ("poslowie.wartosc_wyjazdow")
	private float wyjazdyWartosc;
																					

	@SerializedName ("poslowie.wartosc_biuro_podroze_pracownikow")
	private float kosztPodrozyPracownikowBiura;
	
	@SerializedName ("poslowie.wartosc_biuro_taksowki")
	private float taksowki;
			
	@SerializedName("poslowie.wartosc_biuro_wynagrodzenia_pracownikow")
	private float wynagrodzeniaPracownikow;
	
	
	

	
	
	//wygenerowane przez eclipsa:
	
	

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String[] getKadencjeTab() {
		return kadencjeTab;
	}

	public void setKadencjeTab(String[] kadencjeTab) {
		this.kadencjeTab = kadencjeTab;
	}

	public String getOstatniaKadencja() {
		return ostatniaKadencja;
	}

	public void setOstatniaKadencja(String ostatniaKadencja) {
		this.ostatniaKadencja = ostatniaKadencja;
	}

	public float getBiuroZlecenia() {
		return biuroZlecenia;
	}

	public void setBiuroZlecenia(float biuroZlecenia) {
		this.biuroZlecenia = biuroZlecenia;
	}

	public float getMaterialyBiurowe() {
		return materialyBiurowe;
	}

	public void setMaterialyBiurowe(float materialyBiurowe) {
		this.materialyBiurowe = materialyBiurowe;
	}

	public float getBiuroEkspertyzy() {
		return biuroEkspertyzy;
	}

	public void setBiuroEkspertyzy(float biuroEkspertyzy) {
		this.biuroEkspertyzy = biuroEkspertyzy;
	}

	public float getBiuroTelekomunikacja() {
		return biuroTelekomunikacja;
	}

	public void setBiuroTelekomunikacja(float biuroTelekomunikacja) {
		this.biuroTelekomunikacja = biuroTelekomunikacja;
	}

	public float getBiuroBiuro() {
		return biuroBiuro;
	}

	public void setBiuroBiuro(float biuroBiuro) {
		this.biuroBiuro = biuroBiuro;
	}

	public float getLiczbaPrzelotow() {
		return liczbaPrzelotow;
	}

	public void setLiczbaPrzelotow(float liczbaPrzelotow) {
		this.liczbaPrzelotow = liczbaPrzelotow;
	}

	public float getRefundacjaKwater() {
		return refundacjaKwater;
	}

	public void setRefundacjaKwater(float refundacjaKwater) {
		this.refundacjaKwater = refundacjaKwater;
	}

	public float getBiuroPrzejazdyWartosc() {
		return biuroPrzejazdyWartosc;
	}

	public void setBiuroPrzejazdyWartosc(float biuroPrzejazdyWartosc) {
		this.biuroPrzejazdyWartosc = biuroPrzejazdyWartosc;
	}

	public float getWyjazdyWartosc() {
		return wyjazdyWartosc;
	}

	public void setWyjazdyWartosc(float wyjazdyWartosc) {
		this.wyjazdyWartosc = wyjazdyWartosc;
	}

	public float getKosztPodrozyPracownikowBiura() {
		return kosztPodrozyPracownikowBiura;
	}

	public void setKosztPodrozyPracownikowBiura(float kosztPodrozyPracownikowBiura) {
		this.kosztPodrozyPracownikowBiura = kosztPodrozyPracownikowBiura;
	}

	public float getTaksowki() {
		return taksowki;
	}

	public void setTaksowki(float taksowki) {
		this.taksowki = taksowki;
	}

	public float getWynagrodzeniaPracownikow() {
		return wynagrodzeniaPracownikow;
	}

	public void setWynagrodzeniaPracownikow(float wynagrodzeniaPracownikow) {
		this.wynagrodzeniaPracownikow = wynagrodzeniaPracownikow;
	}
	
	
	//koniec wygenerowanych przez eclipsa
	
	
	
	/*
	@SerializedName()
	
	@SerializedName()
	
	@SerializedName()
	
	
	
	//Wątpliwości:
	
	private String poslowie.wartosc_biuro_srodki_trwale;
	
	private String poslowie.wartosc_uposazenia_pln;
	
	private String poslowie.wartosc_biuro_spotkania;
	
	private String poslowie.liczba_wyjazdow;
	
	private String poslowie.liczba_przejazdow;
	
	//inne, prawdopodownien nieistotne:
	
    private String poslowie.biuro_html;

    private String ludzie.nazwa;

    private String poslowie.nazwa_odwrocona;

    private String poslowie.miejsce_zamieszkania;
    
    private String sejm_kluby.id;

    private String poslowie.okreg_wyborczy_numer;

    private String sejm_kluby.nazwa;

    private String poslowie.klub_id;

    private String poslowie.zawod;

    private String ludzie.slug;

    private String poslowie.frekwencja;

    private String 

    private String poslowie.krs_osoba_id;

    private String ;

    private String poslowie.mowca_id;

    private String poslowie.miejsce_urodzenia;

    private String ;

    private String poslowie.liczba_glosowan;

    private String poslowie.liczba_projektow_uchwal;

    private String poslowie.dopelniacz;

    private String poslowie.okreg_gminy_str;

    private String poslowie.liczba_wypowiedzi;

    private String poslowie.data_urodzenia;

    private String poslowie.liczba_glosow;

    private String poslowie.numer_na_liscie;



    private String poslowie.liczba_uchwal_komisji_etyki;

    private String poslowie.numer_legitymacji;

    private String poslowie.nazwa;

    

    private String poslowie.mandat_wygasl;

    private String poslowie.twitter_account_id;

    private String poslowie.pkw_komitet_id;

    private String poslowie.procent_glosow;

    private String poslowie.wartosc_biuro_inne;

    private String poslowie.liczba_interpelacji;

    

    private String poslowie.liczba_slow;

    private String poslowie.rozliczenie_id;

    private String poslowie.liczba_glosowan_zbuntowanych;

    private String poslowie.pkw_nr_listy;

    

    private String poslowie.liczba_podkomisji;

    

    private String poslowie.liczba_wnioskow;

    private String poslowie.liczba_komisji;

    

    private String poslowie.sejm_okreg_id;

    private String poslowie.liczba_projektow_ustaw;

    private String poslowie.liczba_glosowan_opuszczonych;

    private String poslowie.plec;

    private String poslowie.imiona;

    private String poslowie.zbuntowanie;

    private String sejm_kluby.skrot;
    
    */

	/*
    public String getPoslowie.imie_drugie ()
    {
        return poslowie.imie_drugie;
    }

    public void setPoslowie.imie_drugie (String poslowie.imie_drugie)
    {
        this.poslowie.imie_drugie = poslowie.imie_drugie;
    }

    public String getPoslowie.biuro_html ()
    {
        return poslowie.biuro_html;
    }

    public void setPoslowie.biuro_html (String poslowie.biuro_html)
    {
        this.poslowie.biuro_html = poslowie.biuro_html;
    }

    public String getLudzie.nazwa ()
    {
        return ludzie.nazwa;
    }

    public void setLudzie.nazwa (String ludzie.nazwa)
    {
        this.ludzie.nazwa = ludzie.nazwa;
    }

    public String getPoslowie.nazwa_odwrocona ()
    {
        return poslowie.nazwa_odwrocona;
    }

    public void setPoslowie.nazwa_odwrocona (String poslowie.nazwa_odwrocona)
    {
        this.poslowie.nazwa_odwrocona = poslowie.nazwa_odwrocona;
    }

    public String[] getPoslowie.kadencja ()
    {
        return poslowie.kadencja;
    }

    public void setPoslowie.kadencja (String[] poslowie.kadencja)
    {
        this.poslowie.kadencja = poslowie.kadencja;
    }

    public String getPoslowie.miejsce_zamieszkania ()
    {
        return poslowie.miejsce_zamieszkania;
    }

    public void setPoslowie.miejsce_zamieszkania (String poslowie.miejsce_zamieszkania)
    {
        this.poslowie.miejsce_zamieszkania = poslowie.miejsce_zamieszkania;
    }

    public String getLudzie.id ()
    {
        return ludzie.id;
    }

    public void setLudzie.id (String ludzie.id)
    {
        this.ludzie.id = ludzie.id;
    }

    public String getSejm_kluby.id ()
    {
        return sejm_kluby.id;
    }

    public void setSejm_kluby.id (String sejm_kluby.id)
    {
        this.sejm_kluby.id = sejm_kluby.id;
    }

    public String getPoslowie.okreg_wyborczy_numer ()
    {
        return poslowie.okreg_wyborczy_numer;
    }

    public void setPoslowie.okreg_wyborczy_numer (String poslowie.okreg_wyborczy_numer)
    {
        this.poslowie.okreg_wyborczy_numer = poslowie.okreg_wyborczy_numer;
    }

    public String getPoslowie.wartosc_biuro_zlecenia ()
    {
        return poslowie.wartosc_biuro_zlecenia;
    }

    public void setPoslowie.wartosc_biuro_zlecenia (String poslowie.wartosc_biuro_zlecenia)
    {
        this.poslowie.wartosc_biuro_zlecenia = poslowie.wartosc_biuro_zlecenia;
    }

    public String getPoslowie.nazwisko ()
    {
        return poslowie.nazwisko;
    }

    public void setPoslowie.nazwisko (String poslowie.nazwisko)
    {
        this.poslowie.nazwisko = poslowie.nazwisko;
    }

    public String getSejm_kluby.nazwa ()
    {
        return sejm_kluby.nazwa;
    }

    public void setSejm_kluby.nazwa (String sejm_kluby.nazwa)
    {
        this.sejm_kluby.nazwa = sejm_kluby.nazwa;
    }

    public String getPoslowie.klub_id ()
    {
        return poslowie.klub_id;
    }

    public void setPoslowie.klub_id (String poslowie.klub_id)
    {
        this.poslowie.klub_id = poslowie.klub_id;
    }

    public String getPoslowie.wartosc_biuro_materialy_biurowe ()
    {
        return poslowie.wartosc_biuro_materialy_biurowe;
    }

    public void setPoslowie.wartosc_biuro_materialy_biurowe (String poslowie.wartosc_biuro_materialy_biurowe)
    {
        this.poslowie.wartosc_biuro_materialy_biurowe = poslowie.wartosc_biuro_materialy_biurowe;
    }

    public String getPoslowie.zawod ()
    {
        return poslowie.zawod;
    }

    public void setPoslowie.zawod (String poslowie.zawod)
    {
        this.poslowie.zawod = poslowie.zawod;
    }

    public String getPoslowie.wartosc_biuro_ekspertyzy ()
    {
        return poslowie.wartosc_biuro_ekspertyzy;
    }

    public void setPoslowie.wartosc_biuro_ekspertyzy (String poslowie.wartosc_biuro_ekspertyzy)
    {
        this.poslowie.wartosc_biuro_ekspertyzy = poslowie.wartosc_biuro_ekspertyzy;
    }

    public String getPoslowie.id ()
    {
        return poslowie.id;
    }

    public void setPoslowie.id (String poslowie.id)
    {
        this.poslowie.id = poslowie.id;
    }

    public String getLudzie.slug ()
    {
        return ludzie.slug;
    }

    public void setLudzie.slug (String ludzie.slug)
    {
        this.ludzie.slug = ludzie.slug;
    }

    public String getPoslowie.frekwencja ()
    {
        return poslowie.frekwencja;
    }

    public void setPoslowie.frekwencja (String poslowie.frekwencja)
    {
        this.poslowie.frekwencja = poslowie.frekwencja;
    }

    public String getPoslowie.wartosc_biuro_telekomunikacja ()
    {
        return poslowie.wartosc_biuro_telekomunikacja;
    }

    public void setPoslowie.wartosc_biuro_telekomunikacja (String poslowie.wartosc_biuro_telekomunikacja)
    {
        this.poslowie.wartosc_biuro_telekomunikacja = poslowie.wartosc_biuro_telekomunikacja;
    }

    public String getPoslowie.kadencja_ostatnia ()
    {
        return poslowie.kadencja_ostatnia;
    }

    public void setPoslowie.kadencja_ostatnia (String poslowie.kadencja_ostatnia)
    {
        this.poslowie.kadencja_ostatnia = poslowie.kadencja_ostatnia;
    }

    public String getPoslowie.wartosc_biuro_biuro ()
    {
        return poslowie.wartosc_biuro_biuro;
    }

    public void setPoslowie.wartosc_biuro_biuro (String poslowie.wartosc_biuro_biuro)
    {
        this.poslowie.wartosc_biuro_biuro = poslowie.wartosc_biuro_biuro;
    }

    public String getPoslowie.liczba_przelotow ()
    {
        return poslowie.liczba_przelotow;
    }

    public void setPoslowie.liczba_przelotow (String poslowie.liczba_przelotow)
    {
        this.poslowie.liczba_przelotow = poslowie.liczba_przelotow;
    }

    public String getPoslowie.krs_osoba_id ()
    {
        return poslowie.krs_osoba_id;
    }

    public void setPoslowie.krs_osoba_id (String poslowie.krs_osoba_id)
    {
        this.poslowie.krs_osoba_id = poslowie.krs_osoba_id;
    }

    public String getPoslowie.wartosc_refundacja_kwater_pln ()
    {
        return poslowie.wartosc_refundacja_kwater_pln;
    }

    public void setPoslowie.wartosc_refundacja_kwater_pln (String poslowie.wartosc_refundacja_kwater_pln)
    {
        this.poslowie.wartosc_refundacja_kwater_pln = poslowie.wartosc_refundacja_kwater_pln;
    }

    public String getPoslowie.mowca_id ()
    {
        return poslowie.mowca_id;
    }

    public void setPoslowie.mowca_id (String poslowie.mowca_id)
    {
        this.poslowie.mowca_id = poslowie.mowca_id;
    }

    public String getPoslowie.miejsce_urodzenia ()
    {
        return poslowie.miejsce_urodzenia;
    }

    public void setPoslowie.miejsce_urodzenia (String poslowie.miejsce_urodzenia)
    {
        this.poslowie.miejsce_urodzenia = poslowie.miejsce_urodzenia;
    }

    public String getPoslowie.imie_pierwsze ()
    {
        return poslowie.imie_pierwsze;
    }

    public void setPoslowie.imie_pierwsze (String poslowie.imie_pierwsze)
    {
        this.poslowie.imie_pierwsze = poslowie.imie_pierwsze;
    }

    public String getPoslowie.liczba_glosowan ()
    {
        return poslowie.liczba_glosowan;
    }

    public void setPoslowie.liczba_glosowan (String poslowie.liczba_glosowan)
    {
        this.poslowie.liczba_glosowan = poslowie.liczba_glosowan;
    }

    public String getPoslowie.wartosc_biuro_przejazdy ()
    {
        return poslowie.wartosc_biuro_przejazdy;
    }

    public void setPoslowie.wartosc_biuro_przejazdy (String poslowie.wartosc_biuro_przejazdy)
    {
        this.poslowie.wartosc_biuro_przejazdy = poslowie.wartosc_biuro_przejazdy;
    }

    public String getPoslowie.liczba_projektow_uchwal ()
    {
        return poslowie.liczba_projektow_uchwal;
    }

    public void setPoslowie.liczba_projektow_uchwal (String poslowie.liczba_projektow_uchwal)
    {
        this.poslowie.liczba_projektow_uchwal = poslowie.liczba_projektow_uchwal;
    }

    public String getPoslowie.dopelniacz ()
    {
        return poslowie.dopelniacz;
    }

    public void setPoslowie.dopelniacz (String poslowie.dopelniacz)
    {
        this.poslowie.dopelniacz = poslowie.dopelniacz;
    }

    public String getPoslowie.okreg_gminy_str ()
    {
        return poslowie.okreg_gminy_str;
    }

    public void setPoslowie.okreg_gminy_str (String poslowie.okreg_gminy_str)
    {
        this.poslowie.okreg_gminy_str = poslowie.okreg_gminy_str;
    }

    public String getPoslowie.wartosc_wyjazdow ()
    {
        return poslowie.wartosc_wyjazdow;
    }

    public void setPoslowie.wartosc_wyjazdow (String poslowie.wartosc_wyjazdow)
    {
        this.poslowie.wartosc_wyjazdow = poslowie.wartosc_wyjazdow;
    }

    public String getPoslowie.liczba_wypowiedzi ()
    {
        return poslowie.liczba_wypowiedzi;
    }

    public void setPoslowie.liczba_wypowiedzi (String poslowie.liczba_wypowiedzi)
    {
        this.poslowie.liczba_wypowiedzi = poslowie.liczba_wypowiedzi;
    }

    public String getPoslowie.data_urodzenia ()
    {
        return poslowie.data_urodzenia;
    }

    public void setPoslowie.data_urodzenia (String poslowie.data_urodzenia)
    {
        this.poslowie.data_urodzenia = poslowie.data_urodzenia;
    }

    public String getPoslowie.liczba_glosow ()
    {
        return poslowie.liczba_glosow;
    }

    public void setPoslowie.liczba_glosow (String poslowie.liczba_glosow)
    {
        this.poslowie.liczba_glosow = poslowie.liczba_glosow;
    }

    public String getPoslowie.numer_na_liscie ()
    {
        return poslowie.numer_na_liscie;
    }

    public void setPoslowie.numer_na_liscie (String poslowie.numer_na_liscie)
    {
        this.poslowie.numer_na_liscie = poslowie.numer_na_liscie;
    }

    public String getPoslowie.wartosc_biuro_podroze_pracownikow ()
    {
        return poslowie.wartosc_biuro_podroze_pracownikow;
    }

    public void setPoslowie.wartosc_biuro_podroze_pracownikow (String poslowie.wartosc_biuro_podroze_pracownikow)
    {
        this.poslowie.wartosc_biuro_podroze_pracownikow = poslowie.wartosc_biuro_podroze_pracownikow;
    }

    public String getPoslowie.liczba_uchwal_komisji_etyki ()
    {
        return poslowie.liczba_uchwal_komisji_etyki;
    }

    public void setPoslowie.liczba_uchwal_komisji_etyki (String poslowie.liczba_uchwal_komisji_etyki)
    {
        this.poslowie.liczba_uchwal_komisji_etyki = poslowie.liczba_uchwal_komisji_etyki;
    }

    public String getPoslowie.numer_legitymacji ()
    {
        return poslowie.numer_legitymacji;
    }

    public void setPoslowie.numer_legitymacji (String poslowie.numer_legitymacji)
    {
        this.poslowie.numer_legitymacji = poslowie.numer_legitymacji;
    }

    public String getPoslowie.nazwa ()
    {
        return poslowie.nazwa;
    }

    public void setPoslowie.nazwa (String poslowie.nazwa)
    {
        this.poslowie.nazwa = poslowie.nazwa;
    }

    public String getPoslowie.wartosc_biuro_srodki_trwale ()
    {
        return poslowie.wartosc_biuro_srodki_trwale;
    }

    public void setPoslowie.wartosc_biuro_srodki_trwale (String poslowie.wartosc_biuro_srodki_trwale)
    {
        this.poslowie.wartosc_biuro_srodki_trwale = poslowie.wartosc_biuro_srodki_trwale;
    }

    public String getPoslowie.mandat_wygasl ()
    {
        return poslowie.mandat_wygasl;
    }

    public void setPoslowie.mandat_wygasl (String poslowie.mandat_wygasl)
    {
        this.poslowie.mandat_wygasl = poslowie.mandat_wygasl;
    }

    public String getPoslowie.twitter_account_id ()
    {
        return poslowie.twitter_account_id;
    }

    public void setPoslowie.twitter_account_id (String poslowie.twitter_account_id)
    {
        this.poslowie.twitter_account_id = poslowie.twitter_account_id;
    }

    public String getPoslowie.pkw_komitet_id ()
    {
        return poslowie.pkw_komitet_id;
    }

    public void setPoslowie.pkw_komitet_id (String poslowie.pkw_komitet_id)
    {
        this.poslowie.pkw_komitet_id = poslowie.pkw_komitet_id;
    }

    public String getPoslowie.procent_glosow ()
    {
        return poslowie.procent_glosow;
    }

    public void setPoslowie.procent_glosow (String poslowie.procent_glosow)
    {
        this.poslowie.procent_glosow = poslowie.procent_glosow;
    }

    public String getPoslowie.wartosc_biuro_inne ()
    {
        return poslowie.wartosc_biuro_inne;
    }

    public void setPoslowie.wartosc_biuro_inne (String poslowie.wartosc_biuro_inne)
    {
        this.poslowie.wartosc_biuro_inne = poslowie.wartosc_biuro_inne;
    }

    public String getPoslowie.wartosc_biuro_wynagrodzenia_pracownikow ()
    {
        return poslowie.wartosc_biuro_wynagrodzenia_pracownikow;
    }

    public void setPoslowie.wartosc_biuro_wynagrodzenia_pracownikow (String poslowie.wartosc_biuro_wynagrodzenia_pracownikow)
    {
        this.poslowie.wartosc_biuro_wynagrodzenia_pracownikow = poslowie.wartosc_biuro_wynagrodzenia_pracownikow;
    }

    public String getPoslowie.wartosc_biuro_taksowki ()
    {
        return poslowie.wartosc_biuro_taksowki;
    }

    public void setPoslowie.wartosc_biuro_taksowki (String poslowie.wartosc_biuro_taksowki)
    {
        this.poslowie.wartosc_biuro_taksowki = poslowie.wartosc_biuro_taksowki;
    }

    public String getPoslowie.liczba_interpelacji ()
    {
        return poslowie.liczba_interpelacji;
    }

    public void setPoslowie.liczba_interpelacji (String poslowie.liczba_interpelacji)
    {
        this.poslowie.liczba_interpelacji = poslowie.liczba_interpelacji;
    }

    public String getPoslowie.wartosc_uposazenia_pln ()
    {
        return poslowie.wartosc_uposazenia_pln;
    }

    public void setPoslowie.wartosc_uposazenia_pln (String poslowie.wartosc_uposazenia_pln)
    {
        this.poslowie.wartosc_uposazenia_pln = poslowie.wartosc_uposazenia_pln;
    }

    public String getPoslowie.liczba_slow ()
    {
        return poslowie.liczba_slow;
    }

    public void setPoslowie.liczba_slow (String poslowie.liczba_slow)
    {
        this.poslowie.liczba_slow = poslowie.liczba_slow;
    }

    public String getPoslowie.rozliczenie_id ()
    {
        return poslowie.rozliczenie_id;
    }

    public void setPoslowie.rozliczenie_id (String poslowie.rozliczenie_id)
    {
        this.poslowie.rozliczenie_id = poslowie.rozliczenie_id;
    }

    public String getPoslowie.liczba_glosowan_zbuntowanych ()
    {
        return poslowie.liczba_glosowan_zbuntowanych;
    }

    public void setPoslowie.liczba_glosowan_zbuntowanych (String poslowie.liczba_glosowan_zbuntowanych)
    {
        this.poslowie.liczba_glosowan_zbuntowanych = poslowie.liczba_glosowan_zbuntowanych;
    }

    public String getPoslowie.pkw_nr_listy ()
    {
        return poslowie.pkw_nr_listy;
    }

    public void setPoslowie.pkw_nr_listy (String poslowie.pkw_nr_listy)
    {
        this.poslowie.pkw_nr_listy = poslowie.pkw_nr_listy;
    }

    public String getPoslowie.wartosc_biuro_spotkania ()
    {
        return poslowie.wartosc_biuro_spotkania;
    }

    public void setPoslowie.wartosc_biuro_spotkania (String poslowie.wartosc_biuro_spotkania)
    {
        this.poslowie.wartosc_biuro_spotkania = poslowie.wartosc_biuro_spotkania;
    }

    public String getPoslowie.liczba_podkomisji ()
    {
        return poslowie.liczba_podkomisji;
    }

    public void setPoslowie.liczba_podkomisji (String poslowie.liczba_podkomisji)
    {
        this.poslowie.liczba_podkomisji = poslowie.liczba_podkomisji;
    }

    public String getPoslowie.liczba_wyjazdow ()
    {
        return poslowie.liczba_wyjazdow;
    }

    public void setPoslowie.liczba_wyjazdow (String poslowie.liczba_wyjazdow)
    {
        this.poslowie.liczba_wyjazdow = poslowie.liczba_wyjazdow;
    }

    public String getPoslowie.liczba_wnioskow ()
    {
        return poslowie.liczba_wnioskow;
    }

    public void setPoslowie.liczba_wnioskow (String poslowie.liczba_wnioskow)
    {
        this.poslowie.liczba_wnioskow = poslowie.liczba_wnioskow;
    }

    public String getPoslowie.liczba_komisji ()
    {
        return poslowie.liczba_komisji;
    }

    public void setPoslowie.liczba_komisji (String poslowie.liczba_komisji)
    {
        this.poslowie.liczba_komisji = poslowie.liczba_komisji;
    }

    public String getPoslowie.liczba_przejazdow ()
    {
        return poslowie.liczba_przejazdow;
    }

    public void setPoslowie.liczba_przejazdow (String poslowie.liczba_przejazdow)
    {
        this.poslowie.liczba_przejazdow = poslowie.liczba_przejazdow;
    }

    public String getPoslowie.sejm_okreg_id ()
    {
        return poslowie.sejm_okreg_id;
    }

    public void setPoslowie.sejm_okreg_id (String poslowie.sejm_okreg_id)
    {
        this.poslowie.sejm_okreg_id = poslowie.sejm_okreg_id;
    }

    public String getPoslowie.liczba_projektow_ustaw ()
    {
        return poslowie.liczba_projektow_ustaw;
    }

    public void setPoslowie.liczba_projektow_ustaw (String poslowie.liczba_projektow_ustaw)
    {
        this.poslowie.liczba_projektow_ustaw = poslowie.liczba_projektow_ustaw;
    }

    public String getPoslowie.liczba_glosowan_opuszczonych ()
    {
        return poslowie.liczba_glosowan_opuszczonych;
    }

    public void setPoslowie.liczba_glosowan_opuszczonych (String poslowie.liczba_glosowan_opuszczonych)
    {
        this.poslowie.liczba_glosowan_opuszczonych = poslowie.liczba_glosowan_opuszczonych;
    }

    public String getPoslowie.plec ()
    {
        return poslowie.plec;
    }

    public void setPoslowie.plec (String poslowie.plec)
    {
        this.poslowie.plec = poslowie.plec;
    }

    public String getPoslowie.imiona ()
    {
        return poslowie.imiona;
    }

    public void setPoslowie.imiona (String poslowie.imiona)
    {
        this.poslowie.imiona = poslowie.imiona;
    }

    public String getPoslowie.zbuntowanie ()
    {
        return poslowie.zbuntowanie;
    }

    public void setPoslowie.zbuntowanie (String poslowie.zbuntowanie)
    {
        this.poslowie.zbuntowanie = poslowie.zbuntowanie;
    }

    public String getSejm_kluby.skrot ()
    {
        return sejm_kluby.skrot;
    }

    public void setSejm_kluby.skrot (String sejm_kluby.skrot)
    {
        this.sejm_kluby.skrot = sejm_kluby.skrot;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [poslowie.imie_drugie = "+poslowie.imie_drugie+", poslowie.biuro_html = "+poslowie.biuro_html+", ludzie.nazwa = "+ludzie.nazwa+", poslowie.nazwa_odwrocona = "+poslowie.nazwa_odwrocona+", poslowie.kadencja = "+poslowie.kadencja+", poslowie.miejsce_zamieszkania = "+poslowie.miejsce_zamieszkania+", ludzie.id = "+ludzie.id+", sejm_kluby.id = "+sejm_kluby.id+", poslowie.okreg_wyborczy_numer = "+poslowie.okreg_wyborczy_numer+", poslowie.wartosc_biuro_zlecenia = "+poslowie.wartosc_biuro_zlecenia+", poslowie.nazwisko = "+poslowie.nazwisko+", sejm_kluby.nazwa = "+sejm_kluby.nazwa+", poslowie.klub_id = "+poslowie.klub_id+", poslowie.wartosc_biuro_materialy_biurowe = "+poslowie.wartosc_biuro_materialy_biurowe+", poslowie.zawod = "+poslowie.zawod+", poslowie.wartosc_biuro_ekspertyzy = "+poslowie.wartosc_biuro_ekspertyzy+", poslowie.id = "+poslowie.id+", ludzie.slug = "+ludzie.slug+", poslowie.frekwencja = "+poslowie.frekwencja+", poslowie.wartosc_biuro_telekomunikacja = "+poslowie.wartosc_biuro_telekomunikacja+", poslowie.kadencja_ostatnia = "+poslowie.kadencja_ostatnia+", poslowie.wartosc_biuro_biuro = "+poslowie.wartosc_biuro_biuro+", poslowie.liczba_przelotow = "+poslowie.liczba_przelotow+", poslowie.krs_osoba_id = "+poslowie.krs_osoba_id+", poslowie.wartosc_refundacja_kwater_pln = "+poslowie.wartosc_refundacja_kwater_pln+", poslowie.mowca_id = "+poslowie.mowca_id+", poslowie.miejsce_urodzenia = "+poslowie.miejsce_urodzenia+", poslowie.imie_pierwsze = "+poslowie.imie_pierwsze+", poslowie.liczba_glosowan = "+poslowie.liczba_glosowan+", poslowie.wartosc_biuro_przejazdy = "+poslowie.wartosc_biuro_przejazdy+", poslowie.liczba_projektow_uchwal = "+poslowie.liczba_projektow_uchwal+", poslowie.dopelniacz = "+poslowie.dopelniacz+", poslowie.okreg_gminy_str = "+poslowie.okreg_gminy_str+", poslowie.wartosc_wyjazdow = "+poslowie.wartosc_wyjazdow+", poslowie.liczba_wypowiedzi = "+poslowie.liczba_wypowiedzi+", poslowie.data_urodzenia = "+poslowie.data_urodzenia+", poslowie.liczba_glosow = "+poslowie.liczba_glosow+", poslowie.numer_na_liscie = "+poslowie.numer_na_liscie+", poslowie.wartosc_biuro_podroze_pracownikow = "+poslowie.wartosc_biuro_podroze_pracownikow+", poslowie.liczba_uchwal_komisji_etyki = "+poslowie.liczba_uchwal_komisji_etyki+", poslowie.numer_legitymacji = "+poslowie.numer_legitymacji+", poslowie.nazwa = "+poslowie.nazwa+", poslowie.wartosc_biuro_srodki_trwale = "+poslowie.wartosc_biuro_srodki_trwale+", poslowie.mandat_wygasl = "+poslowie.mandat_wygasl+", poslowie.twitter_account_id = "+poslowie.twitter_account_id+", poslowie.pkw_komitet_id = "+poslowie.pkw_komitet_id+", poslowie.procent_glosow = "+poslowie.procent_glosow+", poslowie.wartosc_biuro_inne = "+poslowie.wartosc_biuro_inne+", poslowie.wartosc_biuro_wynagrodzenia_pracownikow = "+poslowie.wartosc_biuro_wynagrodzenia_pracownikow+", poslowie.wartosc_biuro_taksowki = "+poslowie.wartosc_biuro_taksowki+", poslowie.liczba_interpelacji = "+poslowie.liczba_interpelacji+", poslowie.wartosc_uposazenia_pln = "+poslowie.wartosc_uposazenia_pln+", poslowie.liczba_slow = "+poslowie.liczba_slow+", poslowie.rozliczenie_id = "+poslowie.rozliczenie_id+", poslowie.liczba_glosowan_zbuntowanych = "+poslowie.liczba_glosowan_zbuntowanych+", poslowie.pkw_nr_listy = "+poslowie.pkw_nr_listy+", poslowie.wartosc_biuro_spotkania = "+poslowie.wartosc_biuro_spotkania+", poslowie.liczba_podkomisji = "+poslowie.liczba_podkomisji+", poslowie.liczba_wyjazdow = "+poslowie.liczba_wyjazdow+", poslowie.liczba_wnioskow = "+poslowie.liczba_wnioskow+", poslowie.liczba_komisji = "+poslowie.liczba_komisji+", poslowie.liczba_przejazdow = "+poslowie.liczba_przejazdow+", poslowie.sejm_okreg_id = "+poslowie.sejm_okreg_id+", poslowie.liczba_projektow_ustaw = "+poslowie.liczba_projektow_ustaw+", poslowie.liczba_glosowan_opuszczonych = "+poslowie.liczba_glosowan_opuszczonych+", poslowie.plec = "+poslowie.plec+", poslowie.imiona = "+poslowie.imiona+", poslowie.zbuntowanie = "+poslowie.zbuntowanie+", sejm_kluby.skrot = "+sejm_kluby.skrot+"]";
    }
    
    */
}