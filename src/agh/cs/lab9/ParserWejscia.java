package agh.cs.lab9;

public class ParserWejscia {

	/*
	 * Opracuj system, który na podstawie argumentów linii poleceń wyświetla
	 * następujące informacje (dla określonej kadencji sejmu):
	 * 
	 * suma wydatków posła/posłanki o określonym imieniu i nazwisku wysokości
	 * wydatków na 'drobne naprawy i remonty biura poselskiego' określonego
	 * posła/posłanki średniej wartości sumy wydatków wszystkich posłów
	 * posła/posłanki, który wykonał najwięcej podróży zagranicznych
	 * posła/posłanki, który najdłużej przebywał za granicą posła/posłanki,
	 * który odbył najdroższą podróż zagraniczną listę wszystkich posłów, którzy
	 * odwiedzili Włochy
	 */
	// wejscie:
	// sumaWydatkow Imie Nazwisko kadencja / drobneNaprawy Imie Nazwisko
	// kadencja
	// srednieWydatki kadencja / najwiecejPodrozy kadencja
	// najdluzejZaGranica kadencja / najdrozszaPodroz kadencja /
	// odwiedziliWlochy kadencja / aktualizacja

	public String[] parsujWejscie(String[] argumenty) throws IllegalArgumentException {

		if (argumenty.length < 1) {
			System.out.println("Brak argumentów. ");
			throw new IllegalArgumentException();
		}
		if (argumenty.length > 4) {
			System.out.println("Za dużo argumentów. ");
			throw new IllegalArgumentException();
		}

		String[] instrukcje = new String[argumenty.length];
		
		if (argumenty.length == 4 && (argumenty[0].equals("sumaWydatkow")) || argumenty[0].equals("drobneNaprawy")) {
			instrukcje[0] = argumenty[3]; // chce miec kadencję na pierwszym
											// miejscu
			instrukcje[1] = argumenty[0]; // wlaściwe polecenie
			instrukcje[2] = argumenty[1]; // imie
			instrukcje[3] = argumenty[2]; // nazwisko
			return instrukcje;
		}

		if (argumenty.length == 2 && (
				argumenty[0].equals("srednieWydatki")) || 
				argumenty[0].equals("najwiecejPodrozy")|| 
				argumenty[0].equals("najdluzejZaGranica") || 
				argumenty[0].equals("najdrozszaPodroz") ||
				argumenty[0].equals("odwiedziliWlochy")) {
			instrukcje[0] = argumenty[1]; // chce miec kadencję na pierwszym
											// miejscu
			instrukcje[1] = argumenty[0]; // wlaściwe polecenie
			return instrukcje;
		}
		
		if (argumenty.length == 1 && argumenty[0].equals("aktualizacja")) {
			instrukcje[0] = argumenty[0];
			return instrukcje;
		}
			
		//jak dotrze do tego miejsca to znaczy że coś jest nie tak
		System.out.println("Nie rozpoznano polecenia. ");
		throw new IllegalArgumentException();

	}

}
