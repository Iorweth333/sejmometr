package agh.cs.lab9;

public class Posel {
	public String id;
	public String imie, nazwisko;	
	public String kadencja;
	public float wydatkiSuma;			//"suma wydatków posła/posłanki o określonym imieniu i nazwisku"
	public float drobneNaprawy;		//"wysokości wydatków na 'drobne naprawy i remonty biura poselskiego' określonego posła/posłanki"
	public int liczbaPodrozy;		//"posła/posłanki, który wykonał najwięcej podróży zagranicznych"
									//na razie zakładam że w systemie widnieją tylko zagraniczne, dlatego nie wyszczególniam, ze chodzi o zagraniczna
	public int dniZaGranica;		//"posła/posłanki, który najdłużej przebywał za granicą"
	public float najdrozszaPodroz; 	//"posła/posłanki, który odbył najdroższą podróż zagraniczną"
	public boolean bylWeWloszech;	//"listę wszystkich posłów, którzy odwiedzili Włochy"

	public Posel (String id, String kadencja, String imie, String nazwisko){
		this.id = id;
		this.imie=imie;
		this.nazwisko=nazwisko;
		this.kadencja = kadencja;
		this.wydatkiSuma = 0;
		this.drobneNaprawy = 0;
	}

	public Posel(String kadencja, String imie, String nazwisko) {
		this.imie=imie;
		this.nazwisko=nazwisko;
		this.kadencja = kadencja;
		this.id = "-1";
		this.wydatkiSuma = 0;
		this.drobneNaprawy = 0;
	}
	
	public Posel (String kadencja){
		this.kadencja = kadencja;
		this.id = "-1";
	}
	
	public Posel(String imie, String nazwisko) {
		this.imie=imie;
		this.nazwisko=nazwisko;
	}

	

	public Posel(int liczbaPodrozy, int dniZaGranica, float najdrozszaPodroz) {
		this.liczbaPodrozy = liczbaPodrozy;
		this.dniZaGranica = dniZaGranica;
		this.najdrozszaPodroz = najdrozszaPodroz;
	}

	@Override
	public String toString() {
		return "Posel [id=" + id + ", imie=" + imie + ", nazwisko=" + nazwisko + ", kadencja=" + kadencja
				+ ", wydatkiSuma=" + wydatkiSuma + ", wydatkiDrobne=" + drobneNaprawy + ", liczbaPodrozy="
				+ liczbaPodrozy + ", dniZaGranica=" + dniZaGranica + ", najdrozszaPodroz=" + najdrozszaPodroz
				+ ", bylWeWloszech=" + bylWeWloszech + "]";
	}
}
