package agh.cs.lab9;

import java.util.LinkedList;
import com.google.gson.annotations.SerializedName;

public class Poslowie {
	
	@SerializedName ("Dataobject")
    private LinkedList <Dataobject> listaPoslow;
	//generator stworzył to jako tablicę, ale łączenie tablic byłoby katorgą

    private String Count;

    private Links Links;

    private String Took;	//chyba liczba zaimprotowanych posłów

    
    public void scal (LinkedList <Dataobject> that){
    	this.getListaPoslow().addAll(that);    	
    }
    
    public LinkedList<Dataobject> getListaPoslow() {
		return listaPoslow;
	}


	public void setListaPoslow(LinkedList<Dataobject> listaPoslow) {
		this.listaPoslow = listaPoslow;
	}


	public String getCount ()
    {
        return Count;
    }

    public void setCount (String Count)
    {
        this.Count = Count;
    }

    public Links getLinks ()
    {
        return Links;
    }

    public void setLinks (Links Links)
    {
        this.Links = Links;
    }

    public String getTook ()
    {
        return Took;
    }

    public void setTook (String Took)
    {
        this.Took = Took;
    }
    
    
    
    @Override
    public String toString()
    {
        return "ClassPojo [listaPoslow = "+listaPoslow+", Count = "+Count+", Links = "+Links+", Took = "+Took+"]";
    }
	
	
	
	
}
