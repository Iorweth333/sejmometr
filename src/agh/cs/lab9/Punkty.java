package agh.cs.lab9;
public class Punkty
{
    private String tytul;

    private String numer;

    public String getTytul ()
    {
        return tytul;
    }

    public void setTytul (String tytul)
    {
        this.tytul = tytul;
    }

    public String getNumer ()
    {
        return numer;
    }

    public void setNumer (String numer)
    {
        this.numer = numer;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [tytul = "+tytul+", numer = "+numer+"]";
    }
}