package agh.cs.lab9;
public class Roczniki
{
    private float[] pola;

    private String rok;

    private String dokument_id;

    public float[] getPola ()
    {
        return pola;
    }
    
    public float getPola (int i)
    {
        return pola[i];
    }

    public void setPola (float[] pola)
    {
        this.pola = pola;
    }

    public String getRok ()
    {
        return rok;
    }

    public void setRok (String rok)
    {
        this.rok = rok;
    }

    public String getDokument_id ()
    {
        return dokument_id;
    }

    public void setDokument_id (String dokument_id)
    {
        this.dokument_id = dokument_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [pola = "+pola+", rok = "+rok+", dokument_id = "+dokument_id+"]";
    }
}