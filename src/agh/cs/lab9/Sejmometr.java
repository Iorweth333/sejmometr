package agh.cs.lab9;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.LinkedList;

import static java.lang.System.out;

public class Sejmometr {

	public static void main(String[] args) {

		// najpierw parsownie wejścia- muszę wiedzieć, co mam zrobić

		// potem zadziala Czytacz- pobierze wszystkie dane na temat wszystkich
		// posłów

		// następnie odpale parsowanie danych, które pozostawi tam tylko dane na
		// temat określonego posła lub po prostu odfiltruje te dane których nie
		// potrzebuję
		// to tam bedzie tworzony obiekt (lub obiekty) z klasy Posel- skoro
		// filtruję dane to nie ma sensu żeby coś innego je interpretowało

		// teraz pozostaje już tylko wykonać zasadniczą część zadania
		// zrobią to metody z klasy algorytmy

		// na koniec wypisanie wyniku
		// ale to już w metodzie main, nie ma co kombinować

		String[] instrukcje = null;
		try {
			instrukcje = new ParserWejscia().parsujWejscie(args);
		} catch (IllegalArgumentException ex) {
			out.println("Program wykonuje kilka zadań dotyczących posłów. Wpisz:");
			out.println(
					"sumaWydatkow Imie Nazwisko kadencja - aby uzyskać sume wydatków posła/posłanki o określonym imieniu i nazwisku");
			out.println(
					"drobneNaprawy Imie Nazwisko kadencja - aby uzyskać wysokość wydatków na 'drobne naprawy i remonty biura poselskiego' określonego posła/posłanki");
			out.println("srednieWydatki kadencja - aby uzyskać średnią wartość sumy wydatków wszystkich posłów");
			out.println(
					"najwiecejPodrozy kadencja - aby uzyskać imię i nazwyko posła/posłanki, który wykonał najwięcej podróży zagranicznych ");
			out.println(
					"najdluzejZaGranica kadencja - aby uzyskac imie i nazwisko posła/posłanki, który najdłużej przebywał za granicą");
			out.println(
					"najdrozszaPodroz kadencja - aby uzyskać imie i nazwisko posła/posłanki, który odbył najdroższą podróż zagraniczną ");
			out.println("odwiedziliWlochy kadencja - aby uzyskać listę wszystkich posłów, którzy odwiedzili Włochy");
			out.println("aktualizacja- aby zaktualizować pobrane dane");
			out.println("Wielkość liter oraz kolejność poleceń ma znaczenie.");
			out.println("Obsługiwane kadencje: 7, 8");
			System.exit(1);
			
			//Zero => Everything Okay
			//Positive => Something I expected could potentially go wrong went wrong (bad command-line, can't find file, could not connect to server)
			//Negative => Something I didn't expect at all went wrong (system error - unanticipated exception - externally forced termination e.g. kill -9)
		}

		boolean aktualizacja = false;
		String kadencja = null;
		
		if (instrukcje[0].equals("aktualizacja")) {
			aktualizacja = true;
			kadencja = "8";
		}
			
		else kadencja = instrukcje [0];
		
		if (!instrukcje[0].equals("aktualizacja") && !kadencja.equals("7") && !kadencja.equals("8")){
			out.println("Nieobsługiwana kadencja. Obsługiwane kadencje: 7, 8");
			System.exit(1);
		}
		
		Czytacz czytacz = null;
		
		
		if (aktualizacja == true){
			kadencja = "7";
			try {
				czytacz = new Czytacz("https://api-v3.mojepanstwo.pl/dane/poslowie.json?conditions[poslowie.kadencja]=" + kadencja, kadencja, aktualizacja);
			} catch (MalformedURLException e) {
				out.println("Wystąpił błąd związany z linkiem");
				e.printStackTrace();
				System.exit(1);
			} catch (IOException e) {
				out.println("Wystąpił błąd związany z plikiem");
				e.printStackTrace();
				System.exit(1);
			}
			
			LinkedList <Dataobject> listaPoslow = czytacz.poslowie.getListaPoslow();
			
			try {
				Czytacz.pobierzPlikiWarstw(listaPoslow, "wydatki", true);	//aktualizacja wydatków 7 kadencji
				
				Czytacz.pobierzPlikiWarstw(listaPoslow, "wyjazdy", true);	//aktualizacja wyjazdów 7 kadencji
				
			} catch (MalformedURLException e1) {
				out.println("Podczas aktualizacji wydatków lub wyjazdów wystąpił błąd związany z linkiem");
				e1.printStackTrace();
				System.exit(1);
				
			} catch (IOException e1) {
				out.println("Podczas aktualizacji wydatków lub wyjazdów wystąpił błąd związany z plikiem");
				e1.printStackTrace();
				System.exit(1);
			}	
			
			
			
			
			kadencja = "8";
			try {
				czytacz = new Czytacz("https://api-v3.mojepanstwo.pl/dane/poslowie.json?conditions[poslowie.kadencja]=" + kadencja, kadencja, aktualizacja);
			} catch (MalformedURLException e) {
				out.println("Wystąpił błąd związany z linkiem");
				e.printStackTrace();
				System.exit(1);
			} catch (IOException e) {
				out.println("Wystąpił błąd związany z plikiem");
				e.printStackTrace();
				System.exit(1);
			}
			
			listaPoslow = czytacz.poslowie.getListaPoslow();
			
			try {
				Czytacz.pobierzPlikiWarstw(listaPoslow, "wydatki", true);	//aktualizacja wydatków 8 kadencji
				
				Czytacz.pobierzPlikiWarstw(listaPoslow, "wyjazdy", true);	//aktualizacja wyjazdów 8 kadencji
				
			} catch (MalformedURLException e1) {
				out.println("Podczas aktualizacji wydatków lub wyjazdów wystąpił błąd związany z linkiem");
				e1.printStackTrace();
				System.exit(1);
				
			} catch (IOException e1) {
				out.println("Podczas aktualizacji wydatków lub wyjazdów wystąpił błąd związany z plikiem");
				e1.printStackTrace();
				System.exit(1);
			}	
			
			
			
		}
		
		
		
		//ogólnie nawet jak nie aktualizuję, to muszę zrobić sobie listę posłów dlatego jeszcze raz:
				
		
		try {
			czytacz = new Czytacz("https://api-v3.mojepanstwo.pl/dane/poslowie.json?conditions[poslowie.kadencja]=" + kadencja, kadencja, aktualizacja);
		} catch (MalformedURLException e) {

			out.println("Wystąpił błąd związany z linkiem");
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			out.println("Wystąpił błąd związany z plikiem");
			e.printStackTrace();
			System.exit(1);
		}
		
		LinkedList <Dataobject> listaPoslow = czytacz.poslowie.getListaPoslow();
		
		/*
		 //DEBUG
		out.println("count" + czytacz.poslowie.getCount());
		out.println("took" + czytacz.poslowie.getTook());
		LinkedList <Dataobject> listaPoslow = czytacz.poslowie.getListaPoslow();
		
		for (int i=0; i<listaPoslow.size(); i++){
			String tmp = listaPoslow.get(i).getId();
			out.println("ID: " + tmp);
					
		}
		*/
		
		// wejscie:
		// sumaWydatkow Imie Nazwisko kadencja / drobneNaprawy Imie Nazwisko
		// kadencja
		// srednieWydatki kadencja / najwiecejPodrozy kadencja
		// najdluzejZaGranica kadencja / najdrozszaPodroz kadencja /
		// odwiedziliWlochy kadencja / aktualizacja


		
		
		
		
		if (instrukcje [1].equals("sumaWydatkow")){			
			
			Posel posel = new Posel (instrukcje [0], instrukcje[2], instrukcje[3]);
			
			Algorytmy.wydatkiPosla (posel, listaPoslow);
			
			out.println("Poseł (posłanka) " + instrukcje[2] + " " + instrukcje [3] + " wydał(a) " + posel.wydatkiSuma +"zł");
		}
		
		
		if (instrukcje [1].equals("drobneNaprawy")){
			
			Posel posel = new Posel (instrukcje [0], instrukcje[2], instrukcje[3]);
			
			Algorytmy.wydatkiPosla (posel, listaPoslow);
			
			out.println("Poseł (posłanka) " + instrukcje[2] + " " + instrukcje [3] + " wydał(a) na drobne naprawy i remonty lokalu biura poselskiego " + posel.drobneNaprawy +"zł");
		}
		
		
		
		if (instrukcje [1].equals("srednieWydatki")){
			
			out.println ("Liczę średnie wydatki kadencji " + instrukcje[0]);
			
			float wydatki = 0;
			try {
				wydatki = Algorytmy.podliczSrednieWydatki (listaPoslow);
			} catch (MalformedURLException e) {
				System.out.println("Problem z linkiem podczas pobierania wydatków");
				e.printStackTrace();
				System.exit(1);
			} catch (IOException e) {
				System.out.println("Problem z plikiem podczas pobierania wydatków");
				e.printStackTrace();
				System.exit(1);
			}
			out.println("Posłowie wydali średnio " + wydatki + " zł");			
		}
		
		
		if (instrukcje [1].equals("najwiecejPodrozy")){
			
			out.println ("Szukam najczęstszego podróżnika kadnecji " + instrukcje[0]);
			Posel posel = null;
			
			try {
				posel = Algorytmy.znajdzPodroznika (listaPoslow, "najwiecejPodrozy");
			} catch (MalformedURLException e) {
				System.out.println("Problem z linkiem podczas pobierania wydatków");
				e.printStackTrace();
				System.exit(1);
			} catch (IOException e) {
				System.out.println("Problem z plikiem podczas pobierania wydatków");
				e.printStackTrace();
				System.exit(1);
			}
			
			out.println ("Najwięcej (" + posel.liczbaPodrozy + ")" + " podróży odbył " + posel.imie + " " + posel.nazwisko);
			
			
		}
			
		
		if (instrukcje [1].equals("najdluzejZaGranica")){
			
			out.println ("Szukam posła kadnecji " + instrukcje[0] + " który spędził najwięcej czasu za granicą");
			Posel posel = null;
			
			try {
				posel = Algorytmy.znajdzPodroznika (listaPoslow, "najdluzejZaGranica");
			} catch (MalformedURLException e) {
				System.out.println("Problem z linkiem podczas pobierania wydatków");
				e.printStackTrace();
				System.exit(1);
			} catch (IOException e) {
				System.out.println("Problem z plikiem podczas pobierania wydatków");
				e.printStackTrace();
				System.exit(1);
			}
			
			out.println ("Najwięcej (" + posel.dniZaGranica + ")" + " dni za granicą spędzil " + posel.imie + " " + posel.nazwisko);			
		}
		
		
		if (instrukcje [1].equals("najdrozszaPodroz")){
			
			out.println ("Szukam posła kadnecji " + instrukcje[0] + " który odbył najdłuższą podróż");
			Posel posel = null;
			
			try {
				posel = Algorytmy.znajdzPodroznika (listaPoslow, "najdrozszaPodroz");
			} catch (MalformedURLException e) {
				System.out.println("Problem z linkiem podczas pobierania podrozy");
				e.printStackTrace();
				System.exit(1);
			} catch (IOException e) {
				System.out.println("Problem z plikiem podczas pobierania podrozy");
				e.printStackTrace();
				System.exit(1);
			}
			
			out.println ("Najwięcej (" + posel.najdrozszaPodroz + ")" + " pieniędzy na podróż wydał " + posel.imie + " " + posel.nazwisko);			
		}
			
		
		
			
		if (instrukcje [1].equals("odwiedziliWlochy")){
			
			out.println ("Szukam posłów kadnecji " + instrukcje[0] + " który odwiedzili Włochy");
			LinkedList <Posel> odwiedziliWlochy = null;
			
			try {
				odwiedziliWlochy = Algorytmy.znajdzOdwiedzajacychWlochy (listaPoslow);
			} catch (MalformedURLException e) {
				System.out.println("Problem z linkiem podczas pobierania podrozy");
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("Problem z plikiem podczas pobierania podrozy");
				e.printStackTrace();
			}
			
			out.println ("Włochy odwiedzili:");
			for (Posel odwiedzilWlochy : odwiedziliWlochy){
				
				out.println(odwiedzilWlochy.imie + " " + odwiedzilWlochy.nazwisko);
			}
			
		}
		
	}

}
