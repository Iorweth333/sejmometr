package agh.cs.lab9;

import static java.lang.System.out;
import static org.junit.Assert.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.LinkedList;

import org.junit.Test;


public class Testy {

	//"Program powinien obsługiwać błędy oraz zawierać testy weryfikujące poprawność jego działania."
	
		
	@Test
	public void listaPoslowNieNull() {
		
		Czytacz czytacz = null;
				
		try {
			czytacz = new Czytacz("https://api-v3.mojepanstwo.pl/dane/poslowie.json?conditions[poslowie.kadencja]=" + "8", "8", false);
		} catch (MalformedURLException e) {

			out.println("Wystąpił błąd związany z linkiem");
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			out.println("Wystąpił błąd związany z plikiem");
			e.printStackTrace();
			System.exit(1);
		}
		
		
		LinkedList <Dataobject> listaPoslow = czytacz.poslowie.getListaPoslow();
		
		assertNotNull (listaPoslow);
	}
	
	@Test
	public void znajdzPodroznikaTest1() {
		
		String oczekiwanaOdp = "Najwięcej (45) podróży odbył Jan Dziedziczak";
		
		Czytacz czytacz = null;
		
		try {
			czytacz = new Czytacz("https://api-v3.mojepanstwo.pl/dane/poslowie.json?conditions[poslowie.kadencja]=" + "8", "8", false);
		} catch (MalformedURLException e) {

			out.println("Wystąpił błąd związany z linkiem");
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			out.println("Wystąpił błąd związany z plikiem");
			e.printStackTrace();
			System.exit(1);
		}
		
		
		LinkedList <Dataobject> listaPoslow = czytacz.poslowie.getListaPoslow();
		Posel posel = null;		
		try {
			posel = Algorytmy.znajdzPodroznika(listaPoslow, "najwiecejPodrozy");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String odp = "Najwięcej (" + posel.liczbaPodrozy + ")" + " podróży odbył " + posel.imie + " " + posel.nazwisko;
		assertEquals (odp, oczekiwanaOdp);
		
	}

	
}
