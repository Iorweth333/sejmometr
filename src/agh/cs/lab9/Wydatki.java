package agh.cs.lab9;
public class Wydatki
{
    private Roczniki[] roczniki;

    private int liczba_rocznikow;

    private int liczba_pol;

    private Punkty[] punkty;

    public Roczniki[] getRoczniki ()
    {
        return roczniki;
    }

    public void setRoczniki (Roczniki[] roczniki)
    {
        this.roczniki = roczniki;
    }

    public int getLiczba_rocznikow ()
    {
        return liczba_rocznikow;
    }

    public void setLiczba_rocznikow (int liczba_rocznikow)
    {
        this.liczba_rocznikow = liczba_rocznikow;
    }

    public int getLiczba_pol ()
    {
        return liczba_pol;
    }

    public void setLiczba_pol (int liczba_pol)
    {
        this.liczba_pol = liczba_pol;
    }

    public Punkty[] getPunkty ()
    {
        return punkty;
    }

    public void setPunkty (Punkty[] punkty)
    {
        this.punkty = punkty;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [roczniki = "+roczniki+", liczba_rocznikow = "+liczba_rocznikow+", liczba_pol = "+liczba_pol+", punkty = "+punkty+"]";
    }
}