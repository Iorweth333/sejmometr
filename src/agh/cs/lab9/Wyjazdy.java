package agh.cs.lab9;
public class Wyjazdy
{
	/*
    private String delegacja;

    private float koszt_hotel;

    private float koszt_fundusz;

    private float koszt_dieta;

    private float koszt_ubezpieczenie;
	
	*/
    private String kraj;

    /*
    private float koszt_zaliczki;

    private float koszt_dojazd;

    private String country_code;

    private String id;

    private String wniosek_nr;

    private float koszt_kurs;

    private String od;

    private float koszt_transport;

    private String miasto;

    private String do;

	*/
    private int liczba_dni;

    private float koszt_suma;

    

    public String getKraj() {
		return kraj;
	}



	public void setKraj(String kraj) {
		this.kraj = kraj;
	}



	public int getLiczba_dni() {
		return liczba_dni;
	}



	public void setLiczba_dni(int liczba_dni) {
		this.liczba_dni = liczba_dni;
	}



	public float getKoszt_suma() {
		return koszt_suma;
	}



	public void setKoszt_suma(float koszt_suma) {
		this.koszt_suma = koszt_suma;
	}



	@Override
	public String toString() {
		return "Wyjazdy [kraj=" + kraj + ", liczba_dni=" + liczba_dni + ", koszt_suma=" + koszt_suma + "]";
	}
}